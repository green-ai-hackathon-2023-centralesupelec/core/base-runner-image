login:
	docker login registry.gitlab.com


build_py39:
	docker build -f python3.9-slim-ci/Dockerfile -t registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-ci:latest .
	docker push registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-ci

build_py39_torch:
	docker build -f python3.9-slim-torch2.1-cu121-ci/Dockerfile -t registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-torch2.1-cu121-ci:latest .
	docker push registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-torch2.1-cu121-ci

build_py39_tensorflow:
	docker build -f python3.9-slim-tensorflow2.14-cu121-ci/Dockerfile -t registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-tensorflow2.14-cu121-ci:latest .
	docker push registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-tensorflow2.14-cu121-ci

build_py310:
	docker build -f python3.10-slim-ci/Dockerfile -t registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-ci:latest .
	docker push registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-ci

build_py310_torch:
	docker build -f python3.10-slim-torch2.1-cu121-ci/Dockerfile -t registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-torch2.1-cu121-ci:latest .
	docker push registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-torch2.1-cu121-ci

build_py310_tensorflow:
	docker build -f python3.10-slim-tensorflow2.14-cu121-ci/Dockerfile -t registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-tensorflow2.14-cu121-ci:latest .
	docker push registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-tensorflow2.14-cu121-ci
