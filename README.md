# Base runner image builder

## Build locally

Login to GitLab container registry

```bash
docker login registry.gitlab.com
```

Build docker image

```bash
docker build -t registry.gitlab.com/green-ai-hackathon-2023-centralesupelec/core/base-runner-image/<IMAGE_NAME>:latest
```

Push to container registry

```bash
docker push registry.gitlab.com/green-ai-hackathon-2023-centralesupelec/core/base-runner-image/<IMAGE_NAME>
```